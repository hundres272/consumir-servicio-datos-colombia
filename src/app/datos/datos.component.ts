import { Component, OnInit } from '@angular/core';
import { DatosService } from '../datos.service';
import { DATASALUD } from '../interfaces';

@Component({
  selector: 'app-datos',
  templateUrl: './datos.component.html',
  styleUrls: ['./datos.component.css']
})
export class DatosComponent implements OnInit {

  dataAll: DATASALUD[] = [];
  data: DATASALUD[] = [];
  tamanio: any[] = [];
  active = 0;

  constructor(private datosService: DatosService) { }

  ngOnInit(): void {
    this.datosService.getData()
    .subscribe(res=>{
      this.dataAll = res;
      this.data = this.dataAll.slice(0,50);
      this.tamanio[0] = {
        "class": "page-item active"
      }
      for (let i = 1; i < res.length/50; i++) {
        this.tamanio[i] = {
          "class": "page-item"
        }
      }
    })
  }

  activate(index){
    this.tamanio[index].class = "page-item active";
    this.tamanio[this.active].class = "page-item";
    this.active = index;
    this.data = this.dataAll.slice(index*50,(index*50)+50);
  }
}
