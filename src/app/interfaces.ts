export interface DATASALUD {
    ips:                      IPS;
    fechaasignacionregistro:  Date;
    sexo:                     Sexo;
    edad:                     string;
    hora_salida:              string;
    objeto_remision:          ObjetoRemision;
    impresiones_diagnosticas: string;
}

export enum IPS {
    CSBuesaquillo = "CS BUESAQUILLO",
    CSCabrera = "CS CABRERA",
    CSCatambuco = "CS CATAMBUCO",
    CSEncano = "CS ENCANO",
    CSGenoy = "CS GENOY",
    CSGualmatan = "CS GUALMATAN",
    CSLaguna = "CS LAGUNA",
    CSLorenzo = "CS LORENZO",
    CSObonuco = "CS OBONUCO",
    CSPandiaco = "CS PANDIACO",
    CSPrimeroDeMayo = "CS PRIMERO DE MAYO",
    CSProgreso = "CS PROGRESO",
    CSRosario = "CS ROSARIO",
    CSSANVicente = "CS SAN VICENTE",
    CSSantaBarbara = "CS SANTA BARBARA",
    CSTamasagra = "CS TAMASAGRA",
    CentroHospitalLaRosa = "CENTRO HOSPITAL LA ROSA",
    HospitalLocalCivil = "HOSPITAL LOCAL CIVIL",
    OccUnidadMovilOccidente = "OCC_UNIDAD MOVIL OCCIDENTE",
    PSCaldera = "PS CALDERA",
    PSMapachico = "PS MAPACHICO",
}

export enum ObjetoRemision {
    Cardiologia = "CARDIOLOGIA",
    CirugiaGeneral = "CIRUGIA GENERAL",
    CirugiaPlastica = "CIRUGIA PLASTICA",
    Dermatologia = "DERMATOLOGIA",
    Endocrinologia = "ENDOCRINOLOGIA",
    Fisiatria = "FISIATRIA",
    Gastroenterologia = "GASTROENTEROLOGIA",
    Ginecologia = "GINECOLOGIA",
    MedicinaInterna = "MEDICINA INTERNA",
    Nefrologia = "NEFROLOGIA",
    Neumologia = "NEUMOLOGIA",
    Neurocirugia = "NEUROCIRUGIA",
    Neurologia = "NEUROLOGIA",
    Neuropediatria = "NEUROPEDIATRIA",
    Neuropsicologia = "NEUROPSICOLOGIA",
    Nutricion = "NUTRICION",
    Oftalmologia = "OFTALMOLOGIA",
    Optometria = "OPTOMETRIA",
    Ortopedia = "ORTOPEDIA",
    Otorrinolaringologia = "OTORRINOLARINGOLOGIA",
    Otro = "OTRO",
    Pediatria = "PEDIATRIA",
    Pomeroy = "POMEROY",
    Psicologia = "PSICOLOGIA",
    Psiquiatria = "PSIQUIATRIA",
    Reumatologia = "REUMATOLOGIA",
    TerapiaFisicaYRehabilitacion = "TERAPIA FISICA Y REHABILITACION",
    Urologia = "UROLOGIA",
}

export enum Sexo {
    F = "F",
    M = "M",
}